﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Arphox.MouseManipulator")]
[assembly: AssemblyDescription("A .NET 4.5 library for Windows to simulate mouse movements like clicking and scrolling the mouse wheel.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Károly Ozsvárt (Arphox)")]
[assembly: AssemblyProduct("Arphox.MouseManipulator")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("4470607b-b1e6-4d40-3211-9f69ec32e11c")]

[assembly: AssemblyVersion("1.3.0.0")]