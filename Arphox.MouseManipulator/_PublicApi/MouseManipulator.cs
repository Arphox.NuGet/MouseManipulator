﻿using Arphox.MouseManipulator.Internal;
using System;

namespace Arphox.MouseManipulator
{
    public static class MouseManipulator
    {
        /// <summary> Returns the cursor's current position. </summary>
        public static IntegerPoint GetCursorPosition()
        {
            if (!MouseManipulatorInternal.GetCursorPos_DLL(out IntegerPoint point))
                throw new InvalidOperationException("Could not retrieve the mouse cursor's current position.");

            return new IntegerPoint(point.X, point.Y);
        }

        /// <summary> Teleports the cursor to the given coordinates. </summary>
        public static void SetCursorPosition(int x, int y)
            => MouseManipulatorInternal.SetCursorPosition(x, y);

        /// <summary> Performs a left mouse down in the cursor's current position. </summary>
        public static void LeftMouseDown()
            => MouseManipulatorInternal.DoLeftMouseDown();

        /// <summary> Performs a left mouse down  at the given coordinates. </summary>
        public static void LeftMouseDown(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoLeftMouseDown();
        }

        /// <summary> Performs a left mouse up in the cursor's current position. </summary>
        public static void LeftMouseUp()
            => MouseManipulatorInternal.DoLeftMouseUp();

        /// <summary> Performs a left mouse up  at the given coordinates. </summary>
        public static void LeftMouseUp(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoLeftMouseUp();
        }

        /// <summary> Performs a left click in the cursor's current position. </summary>
        public static void LeftClick()
            => MouseManipulatorInternal.DoLeftClick();

        /// <summary> Performs a left click at the given coordinates. </summary>
        public static void LeftClick(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoLeftClick();
        }

        /// <summary> Performs a right mouse down in the cursor's current position. </summary>
        public static void RightMouseDown()
            => MouseManipulatorInternal.DoRightMouseDown();

        /// <summary> Performs a right mouse down  at the given coordinates. </summary>
        public static void RightMouseDown(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoRightMouseDown();
        }

        /// <summary> Performs a right mouse up in the cursor's current position. </summary>
        public static void RightMouseUp()
            => MouseManipulatorInternal.DoRightMouseUp();

        /// <summary> Performs a right mouse up  at the given coordinates. </summary>
        public static void RightMouseUp(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoRightMouseUp();
        }

        /// <summary> Performs a right click in the cursor's current position. </summary>
        public static void RightClick()
            => MouseManipulatorInternal.DoRightClick();

        /// <summary> Performs a right click at the given coordinates. </summary>
        public static void RightClick(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoRightClick();
        }

        /// <summary> Performs a middle mouse down in the cursor's current position. </summary>
        public static void MiddleMouseDown()
            => MouseManipulatorInternal.DoMiddleMouseDown();

        /// <summary> Performs a middle mouse down  at the given coordinates. </summary>
        public static void MiddleMouseDown(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoMiddleMouseDown();
        }

        /// <summary> Performs a middle mouse up in the cursor's current position. </summary>
        public static void MiddleMouseUp()
            => MouseManipulatorInternal.DoMiddleMouseUp();

        /// <summary> Performs a middle mouse up  at the given coordinates. </summary>
        public static void MiddleMouseUp(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoMiddleMouseUp();
        }

        /// <summary> Performs a middle click in the cursor's current position. </summary>
        public static void MiddleClick()
            => MouseManipulatorInternal.DoMiddleClick();

        /// <summary> Performs a middle click at the given coordinates. </summary>
        public static void MiddleClick(int x, int y)
        {
            MouseManipulatorInternal.SetCursorPosition(x, y);
            MouseManipulatorInternal.DoMiddleClick();
        }

        /// <summary>
        ///     Scrolls the mouse wheel up by the given ticks.
        /// </summary>
        /// <param name="ticks">The number of ticks to scroll, can be in range [-100; 100]</param>
        public static void ScrollMouseWheelUp(int ticks)
            => MouseManipulatorInternal.Scroll(ticks);

        /// <summary>
        ///     Scrolls the mouse wheel down by the given ticks.
        /// </summary>
        /// <param name="ticks">The number of ticks to scroll, can be in range [-100; 100]</param>
        public static void ScrollMouseWheelDown(int ticks)
            => MouseManipulatorInternal.Scroll(-ticks);
    }
}