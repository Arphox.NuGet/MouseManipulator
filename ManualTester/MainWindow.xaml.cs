﻿using Arphox.MouseManipulator;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ManualTester
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public int WantedX { get; set; }
        public int WantedY { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Q: LeftMouseDown(e.KeyboardDevice.Modifiers); break;
                case Key.W: LeftMouseUp(e.KeyboardDevice.Modifiers); break;
                case Key.E: LeftMouseClick(e.KeyboardDevice.Modifiers); break;

                case Key.R: RightMouseDown(e.KeyboardDevice.Modifiers); break;
                case Key.T: RightMouseUp(e.KeyboardDevice.Modifiers); break;
                case Key.Z: RightMouseClick(e.KeyboardDevice.Modifiers); break;

                case Key.U: MiddleMouseDown(e.KeyboardDevice.Modifiers); break;
                case Key.I: MiddleMouseUp(e.KeyboardDevice.Modifiers); break;
                case Key.O: MiddleMouseClick(e.KeyboardDevice.Modifiers); break;

                case Key.A: GetCursorPos(); break;
                case Key.S: MouseManipulator.SetCursorPosition(WantedX, WantedY); break;

                case Key.D: MouseManipulator.ScrollMouseWheelDown(1); break;
                case Key.F: MouseManipulator.ScrollMouseWheelUp(1); break;

                case Key.Escape: Close(); break;
            }
        }

        private void LeftMouseDown(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.LeftMouseDown();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.LeftMouseDown(WantedX, WantedY);
        }
        private void LeftMouseUp(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.LeftMouseUp();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.LeftMouseUp(WantedX, WantedY);
        }
        private void LeftMouseClick(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.LeftClick();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.LeftClick(WantedX, WantedY);
        }

        private void RightMouseDown(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.RightMouseDown();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.RightMouseDown(WantedX, WantedY);
        }
        private void RightMouseUp(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.RightMouseUp();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.RightMouseUp(WantedX, WantedY);
        }
        private void RightMouseClick(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.RightClick();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.RightClick(WantedX, WantedY);
        }

        private void MiddleMouseDown(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.MiddleMouseDown();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.MiddleMouseDown(WantedX, WantedY);
        }
        private void MiddleMouseUp(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.MiddleMouseUp();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.MiddleMouseUp(WantedX, WantedY);
        }
        private void MiddleMouseClick(ModifierKeys modifiers)
        {
            if (modifiers == ModifierKeys.None)
                MouseManipulator.MiddleClick();
            else if (modifiers == ModifierKeys.Control)
                MouseManipulator.MiddleClick(WantedX, WantedY);
        }

        private void GetCursorPos()
        {
            IntegerPoint position = MouseManipulator.GetCursorPosition();
            WantedX = position.X; RaisePropertyChanged(nameof(WantedX));
            WantedY = position.Y; RaisePropertyChanged(nameof(WantedY));
        }

        private void ButtonDownUpDetector_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Button button = (Button)sender;
            button.Content = $"{GetPressedMouseButton(e)} Mouse down";
        }

        private void ButtonDownUpDetector_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            Button button = (Button)sender;
            button.Content = "Mouse up";
        }

        private string GetPressedMouseButton(MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) return "Left";
            if (e.RightButton == MouseButtonState.Pressed) return "Right";
            if (e.MiddleButton == MouseButtonState.Pressed) return "Middle";

            return "?";
        }

        private void ButtonDownUpDetector_MouseLeave(object sender, MouseEventArgs e)
            => ((Button)sender).Content = "";

        private void ButtonClickCounter_PreviewMouseDownOrUp(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            button.Content = int.Parse(button.Content.ToString()) + 1;
        }

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void ButtonScrollDetector_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Button button = (Button)sender;
            button.Content = int.Parse(button.Content.ToString()) + (e.Delta / 120);
        }
    }
}