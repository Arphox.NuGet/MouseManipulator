@ECHO OFF
ECHO ------------------------------------------------------------------------
ECHO If you don't have nuget,download from here: https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
ECHO And add it to your PATH.
ECHO ------------------------------------------------------------------------
nuget pack ".\Arphox.MouseManipulator\Arphox.MouseManipulator.csproj" -Build -Properties Configuration=Release
pause