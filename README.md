# Arphox.MouseManipulator
A mouse manipulator .NET library for Windows by Károly Ozsvárt (Arphox).  

Minimum supported Windows version: Windows 2000 Professional/Server  
(So **should work on anything later than Windows 2000**)

## Features

- Simulate:
  - left / middle / right clicks
  - scroll up / scroll down
- Get / set the cursor's current position.

## Examples

After installing the NuGet package, add the following using:  
`using Arphox.MouseManipulator;`

### GetCursorPosition
```csharp
IntegerPoint location = MouseManipulator.GetCursorPosition();
Console.WriteLine($"Cursor is at ({location.X}, {location.Y})");
```

### LeftClick
Performs a left mouse click at the mouse's current position.
```csharp
MouseManipulator.LeftClick();
```

## How can I get it?

**Use the NuGet package manager**,  
or download directly from nuget.org:  
https://www.nuget.org/packages/Arphox.MouseManipulator/