﻿using Arphox.MouseManipulator;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sut = Arphox.MouseManipulator.MouseManipulator;

namespace UnitTest
{
    [TestClass]
    public sealed class AcceptanceTests
    {
        [TestMethod]
        public void GetCursorPosition_ShouldReturnNonNegativePoint()
        {
            // Arrange & Act
            IntegerPoint result = Sut.GetCursorPosition();

            // Assert
            result.X.Should().BeGreaterOrEqualTo(0);
            result.Y.Should().BeGreaterOrEqualTo(0);
        }

        [TestMethod]
        public void SetCursorPosition_Works()
        {
            // Arrange
            const int X = 200;
            const int Y = 300;
            Sut.SetCursorPosition(X, Y);

            // Act
            IntegerPoint result = Sut.GetCursorPosition();

            // Assert
            result.X.Should().Be(X);
            result.Y.Should().Be(Y);
        }
    }
}